#include <stdio.h>
#include <ctime>
#include <stdlib.h>
#include "haar.h"
#include "image.h"
#include <hls_stream.h>
#include <ap_axi_sdata.h>

typedef ap_axiu<32,2,5,6> AXI_VALUE;

using namespace std;

struct FaceStructure
{
	int x;
	int y;
	int width;
	int height;
};
typedef struct FaceStructure Face;

typedef struct
{
	Face face1;
	Face face2;
	Face face3;
	Face face4;
} Faces;


Faces detect(hls::stream<AXI_VALUE> &in_stream)
{

#pragma HLS INTERFACE s_axilite port=return bundle=CONTROL_BUS
#pragma HLS INTERFACE axis port=in_stream name=INPUT_STREAM
	Faces detectionResult;
 
  int flag;
  int in_flag , in_width , in_height , in_maxgrey;
  int ret_val=1;
  int i;
  unsigned char Data[240][320];
  AXI_VALUE aValue;

  read_A: for(i=0; i< 240*320; i++){
  #pragma HLS PIPELINE
  		in_stream.read(aValue);
  		Data[i % 240][i / 240] = aValue.data;
  }


  // Arguments to be passed to DUT	
  MyRect result[RESULT_SIZE];
  int result_x[RESULT_SIZE];
  int result_y[RESULT_SIZE];
  int result_w[RESULT_SIZE];
  int result_h[RESULT_SIZE];

  int res_size=0;
  int *result_size = &res_size;


  // As the SDSoC generated data motion network does not support sending 320 X 240 images at once
  // We needed to send all the 240 rows using 240 iterations. The last invokation of detectFaces() does the actual face detection

  for ( i = 0; i < IMAGE_HEIGHT-1; i+=1 ){
      detectFaces ( Data[i], result_x, result_y, result_w, result_h, result_size);
  }


  for (int j = 0; j < RESULT_SIZE ; j++){
	  Face f;
	  f.x = result_x[j];
	  f.y = result_y[j];
	  f.width = result_w[j];
	  f.height = result_h[j];

	  switch(i)
	  {
	  case 0:
	  		  detectionResult.face1=f;
	  		  break;

	  case 1:
	  		  detectionResult.face2=f;
	  		  break;

		case 2:
			  detectionResult.face3=f;
			  break;

		case 3:
			  detectionResult.face4=f;
			  break;
	  }
  }
	

   


  return detectionResult;

}
